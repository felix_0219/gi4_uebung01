#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char* argv[], char* envp[]) 
{
	char* arg1;
	char* arg2;
	char* var;

	if (argc == 1)
	{
		printf("ERROR: No arguments.\n");
	}
	else if (argc == 2)
	{
		arg1 = argv[1];
		var = getenv(arg1);
		
		if (var)
			return (0);
	}
	else if (argc == 3)
	{
		arg1 = argv[1];
		arg2 = argv[2];

		if (!strcmp(arg1, "-v"))
		{
			var = getenv(arg2);
			if (var)
			{
				printf("%s\n", var);
				return (0);
			}
		}
		else if (!strcmp(arg2, "-v"))
		{
			var = getenv(arg1);
			if (var)
			{
				printf("%s\n", var);
				return(0);
			}
		}
		else
			printf("ERROR: Unknown Argument.\n");
	}
	else 
	{
		printf("ERROR: To many arguments.\n");
	}

	return(1);
}
