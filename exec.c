#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char* argv[], char* envp[]) 
{
	if (argc == 1)
	{
		printf("ERROR: No no argument.\n");
		return(1);
	}
	
	pid_t pid = fork();
	int status;

	if (pid == 0)
	{
		printf("Childprozess\n");
		exit(execv(argv[1], &argv[1]));
	}
	else if (pid > 1)
	{
		printf("Parentprozess\n");
		pid = wait(&status);
		return (status);
	}
	else 
	{
		printf("ERROR: Unable to create childprozess\n");
		return (1);
	}

	return (0);
}
